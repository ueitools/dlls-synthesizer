using System;
using System.Collections.Generic;
using System.Text;

namespace UEI
{
    public class Synthesizer
    {
        private const int _ADDKEY = 0x1C00;
        private const int _ROTIMES = 3;
        private const int _SUBADJ = 28;

        /// <summary>
        /// Assume only one and two byte data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string getCode(string data)
        {
            int value;

            try
            {
                value = Convert.ToInt32(data, 2);
            }
            catch (FormatException ex)
            {
                return "00000";
            }
            if (data.Length == 8)
                value = getCodeForByte(value);
            else if (data.Length == 16)
                value = getCodeForTwoByte(value);

            // for 3 and 4 byte data just send value
            return value.ToString("00000");
        }

        ///
        ///
        ///
        private static int getCodeForByte(int value)
        {
            value = value & 0x00FF;
            const int XORKEY = 197;
            value ^= XORKEY;

            // rotate the value by shifting left then moving upper bits to lower bits
            value <<= _ROTIMES;
            value = value - ((value / 256) * 256) + (value / 256);

            // calculate the parity
            int temp = value;
            int parity = 0;
            for (int i = 0; i < 8; i++)
            {
                if ((temp & 0x80) != 0)
                    parity ^= 1;

                temp <<= 1;
            }

            // set the high order bit to equal the parity bit
            // perform subtraction adjustment
            value += (256 * parity) + 256;
            value -= _SUBADJ;
            return value;
        }

        private static int getCodeForTwoByte(int value)
        {
            const int XORKEY = 0xc5c5;
            value ^= XORKEY;

            // rotate the value by shifting left then moving upper bits to lower bits
            int hiByte = (value & 0xFF00) >> 8;
            hiByte <<= _ROTIMES;
            hiByte = hiByte - ((hiByte / 256) * 256) + (hiByte / 256);
            hiByte <<= 8;

            value = ((value & 0x00FF) | hiByte);
            value += _ADDKEY;
            return value;
        }
    }
}
